#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 16:45:22 2021

@author: hamm495
"""
import os
import sys

try:
    pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
    print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
    sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import subprocess
import pflotran as pft
from error_integrator import *

params_filename = ''
result_filename = ''
dict_ = {}
if len(sys.argv) > 2:
    params_filename = sys.argv[1]
    result_filename = sys.argv[2]
    f = open(params_filename,'r')
    num_variables = int(f.readline().strip().split()[0])
    for i in range(num_variables):
        words = f.readline().strip().split()
        dict_[words[1]] = words[0]
    f.close()
else:
    dict_['X_VELOCITY'] = sys.argv[1]

# Katie #1: update prefix
prefix = 'tracer'
filename = prefix + '.template'
fin = open(filename,'r')
fout = open(filename.split('.')[0]+'.in','w')
for line in fin:
    for key,value in dict_.items():
        line = line.replace(key,value)
    fout.write(line)
fin.close()
fout.close()

command = []
command.append(pflotran_dir+'/src/pflotran/pflotran')
command.append('-input_prefix')
command.append(prefix)
filename = 'pflotran.stdout'
proc = subprocess.run(command,
                      shell=False,
                      stdout=open(filename,'w'),
                      stderr=subprocess.STDOUT)
#                        stderr=subprocess.STDOUT)

# Katie #2: update column id (icol)
icol = 2
data = pft.Dataset(prefix+'-obs-0.pft.gold',1,icol)
times1 = data.get_array('x')
values1 = data.get_array('y')


data = pft.Dataset(prefix+'-obs-0.pft',1,icol)
times2 = data.get_array('x')
values2 = data.get_array('y')

integrator = ErrorIntegrator()
m,a = integrator.calculate_error(times1,values1,times2,values2)
if len(result_filename) > 0:
    fout = open(result_filename,'w')
    fout.write('{}'.format(m))
    fout.close()
else:
    print(m,a)
