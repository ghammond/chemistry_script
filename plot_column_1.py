import sys
import os
try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import pflotran as pft
import re
from extract_column_data import ReadColumnData

if len(sys.argv) < 2:
    print('\n A PFLOTRAN observation filename must be entered on the '
          'command line. E.g.\n\n'
          '    python %s <filename>\n'%sys.argv[0])
    sys.exit(0)

filename = sys.argv[1]
if not os.path.isfile(filename):
    print('File "%s" does not exist within directory "%s".'
          %(filename,os.getcwd()))
    sys.exit(0)

matplotlib.rcParams.update({'font.size': 8})
f = plt.figure(figsize=(10,8))
f.suptitle('Column #1',fontsize=12)

#xlabel = 'Time [d]'
xlabel = 'Pore Volume'
ylabel = 'Concentration [M]'
yscale = 'linear'

nplotx = 3
nploty = 3
pv_per_day = 6.65
filenames = [filename]

debug = False

def plot_concentration(iplot,xlabel,ylabel,yscale,col_id):
    plt.subplot(nplotx,nploty,iplot)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.yscale(yscale)
    data = pft.Dataset(filenames[0],1,col_id)
    yname = data.get_name('yname')
    if debug:
        print(yname)
    yname = re.sub('\A[0-9]*-','',yname)
    yname = re.split('\[M\]|VF',yname)[0]
    if debug:
        print('{} {}'.format(col,yname))
    plt.title(yname)
    yvalues = data.get_array('y')
    ymin,ymax = get_bounds(yvalues)
    xvalues = data.get_array('x')
    if xlabel == 'Pore Volume':
        xvalues *= pv_per_day
    plt.plot(xvalues,yvalues)
   # for adding experimental data 
    if yname.startswith('Total'):
        species = yname.split()[1].strip()
        pv,conc = ReadColumnData('Column1_Effluent.csv',species)
        plt.plot(pv,conc)

    
def get_bounds(array):
    minval = 1e20
    maxval = -minval
    minval = min(minval,min(array))
    maxval = max(maxval,max(array))    
    return minval, maxval


columns = [3,5,9,10,11,12,18]
i = 0
for col in columns:
    i += 1
    plot_concentration(i,xlabel,ylabel,yscale,col)
    
columns = [20,21]
ylabel = 'Volume Fraction [-]'
for col in columns:
    i += 1
    plot_concentration(i,xlabel,ylabel,yscale,col)

f.subplots_adjust(hspace=0.35,wspace=0.45,
                  bottom=.05,top=.92,
                  left=.12,right=.98)

plt.show()
