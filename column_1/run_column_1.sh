#!/bin/sh

REPO_PATH=$(git rev-parse --show-toplevel)
python3 $REPO_PATH/scripts/driver.py \
	--default-parameters default_parameters.txt \
	--pflotran-prefix pt_column_1_v2 \
        --experimental-results-filename ../data/Column1_Effluent.csv \
	--dakota-parameters-filename params.in \
	--dakota-results-filename results.out
