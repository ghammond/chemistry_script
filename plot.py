import sys
import os
try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import pflotran as pft

path = []
path.append('.')

filenames = ['column_2-obs-0.pft']

f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("",fontsize=16)
plt.xlabel('Time [d]')
plt.ylabel('Concentration [M]')

#plt.xlim(0.,1.)
#plt.ylim(4.8,8.2)
#plt.grid(True)
plt.yscale('log')

columns = np.arange(1,20)
columns = columns + 1

columns = [3,9,11,12,18]

for col in columns:
  data = pft.Dataset(filenames[0],1,col)
  yname = data.get_name('yname').split('[M]')[0]
  print('{} {}'.format(col,yname))
  plt.plot(data.get_array('x'),data.get_array('y'),label=yname)

#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
plt.legend(loc=4,title='Time [y]')
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
#plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.gca().get_legend().get_frame().set_fill(False)
plt.gca().get_legend().draw_frame(False)
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

plt.twinx()
plt.ylabel('Volume Fraction [-]')
columns = [20]
minval = 1e20
maxval = -minval
for col in columns:
  data = pft.Dataset(filenames[0],1,col)
  yname = data.get_name('yname').split('VF')[0]
  print('{} {}'.format(col,yname))
  y_values = data.get_array('y')
  minval = min(minval,min(y_values))
  maxval = max(maxval,max(y_values))
  plt.plot(data.get_array('x'),y_values,label=yname)

#plt.ylim(minval,maxval)
#print(minval,maxval)

plt.legend(loc=7)
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
#plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.gca().get_legend().get_frame().set_fill(False)
plt.gca().get_legend().draw_frame(False)

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

plt.show()
