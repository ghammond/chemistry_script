#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 10:34:56 2020

@author: gehammo
"""
import sys
import re
import numpy as np

def read_float_safe(string):
    return float(re.sub(r'[d,D]','e',string))

def string_to_species(strings):
    species_tuples = []
    for i in range(0,len(strings),2):
        species_tuples.append((strings[i+1].strip("'"),read_float_safe(strings[i])))
    return species_tuples

class PrimaryAqueousSpecies():
    def __init__(self,name,info):
        self.name = name
        w = info.split()
        self.size = read_float_safe(w[0])
        self.charge = read_float_safe(w[1])
        self.weight = read_float_safe(w[2]) 
        self.species_tuple = []
        self.dbase_species_tuple = self.species_tuple 
        self.swapped_species = []
        
    def match(self,list_of_primary):
        for primary in list_of_primary:
            if self.name.startswith(primary):
                return True
        if len(self.species_tuple) > 0:
            for species, stoich in self.species_tuple:
                found = False
                for primary in list_of_primary:
                    if species.startswith(primary):
                        found = True
                        break
                if not found:
                    return False
            return True
        return False
    
    def get_species_tuple(self):
        return self.species_tuple
    
    def print_species(self):
        strings = []
        strings.append(self.name)
        if len(self.species_tuple) > 0:
            strings.append(' : ')
            for name, stoich in self.dbase_species_tuple:
                strings.append(' {} {}'.format(stoich,name))
        return ''.join(strings)
        
    def info(self):
        sys.stdout.write('{} {} {} {}\n'.format(self.name,self.size,self.charge,self.weight))
    
    def substitute_species_in_tuple(self,species_to_substitute):
        self.swapped_species.append(species_to_substitute.name)
        species_dict = {}
        sub_species_stoich = 0.
        for name, stoich in self.species_tuple:
            if name == species_to_substitute.name:
                sub_species_stoich = stoich
            else:
                species_dict[name] = stoich
        for name, stoich in species_to_substitute.get_species_tuple():
            if name in species_dict:
                species_dict[name] += stoich*sub_species_stoich
            else:
                species_dict[name] = stoich*sub_species_stoich
        new_tuple = []
        for key in species_dict:
            if abs(species_dict[key]) > 1.e-10: # remove stoichiometries of zero
                new_tuple.append((key,species_dict[key]))
        self.species_tuple = new_tuple
        
    def swap_species(self,dict_,redox_species):
        num_swapped = 0
        for name, stoich in self.get_species_tuple():
            if name in dict_ and not name in redox_species:
                num_swapped += 1
                self.substitute_species_in_tuple(dict_[name])
        return num_swapped
    
    def insert_swapped_gases(self,swapped_gas_dict,dbase_gas_dict):
        # swapped species may not be gases
        for name in self.swapped_species:
            if name in dbase_gas_dict:
                swapped_gas_dict[name] = True
    
class SecondaryAqueousSpecies(PrimaryAqueousSpecies):
    def __init__(self,name,info,num_temperatures):
        self.name = name
        w = info.split(None,1)
        self.num_species = int(w[0])
        info = w[1].strip()
        w = info.split(None,self.num_species*2)
        info = w[self.num_species*2]
        self.species_tuple = string_to_species(w[0:self.num_species*2])
        self.dbase_species_tuple = self.species_tuple 
        w = info.split(None,num_temperatures)
        self.logKs = w[0:-1]
        info = w[-1]
        w = info.strip().split()
        self.size = read_float_safe(w[0])
        self.charge = read_float_safe(w[1])
        self.weight = read_float_safe(w[2])   
        self.swapped_species = []
        
    def info(self):
        sys.stdout.write('{} {}'.format(self.name,self.num_species))
        for name, stoich in self.dbase_species_tuple:
            sys.stdout.write(' {} {}'.format(stoich,name))
        for logK in self.logKs:
            sys.stdout.write(' {}'.format(logK))
        sys.stdout.write(' {} {} {}\n'.format(self.size,self.charge,self.weight))

    
class GasSpecies(PrimaryAqueousSpecies):
    def __init__(self,name,info,num_temperatures):
        self.name = name
        w = info.split(None,2)
        self.molar_volume = read_float_safe(w[0])
        self.num_species = int(w[1])
        info = w[2].strip()
        w = info.split(None,self.num_species*2)
        info = w[self.num_species*2]
        self.species_tuple = string_to_species(w[0:self.num_species*2])
        self.dbase_species_tuple = self.species_tuple 
        w = info.split(None,num_temperatures)
        self.logKs = w[0:-1]
        info = w[-1].strip()
        self.weight = read_float_safe(info.split()[0])
        self.swapped_species = []
        
    def info(self):
        sys.stdout.write('{} {} {}'.format(self.name,self.molar_volume,self.num_species))
        for name, stoich in self.species_tuple:
            sys.stdout.write(' {} {}'.format(stoich,name))
        for logK in self.logKs:
            sys.stdout.write(' {}'.format(logK))
        sys.stdout.write(' {}\n'.format(self.weight))

class MineralSpecies(PrimaryAqueousSpecies):
    def __init__(self,name,info,num_temperatures):
        self.name = name
        w = info.split(None,2)
        self.molar_volume = read_float_safe(w[0])
        self.num_species = int(w[1])
        info = w[2].strip()
        w = info.split(None,self.num_species*2)
        info = w[self.num_species*2]
        self.species_tuple = string_to_species(w[0:self.num_species*2])
        self.dbase_species_tuple = self.species_tuple 
        w = info.split(None,num_temperatures)
        self.logKs = w[0:-1]
        info = w[-1].strip()
        self.weight = read_float_safe(info.split()[0]) 
        self.swapped_species = []
        
    def info(self):
        sys.stdout.write('{} {} {}'.format(self.name,self.molar_volume,self.num_species))
        for name, stoich in self.dbase_species_tuple:
            sys.stdout.write(' {} {}'.format(stoich,name))
        for logK in self.logKs:
            sys.stdout.write(' {}'.format(logK))
        sys.stdout.write(' {}\n'.format(self.weight))

class SurfaceComplexSpecies(PrimaryAqueousSpecies):
    def __init__(self,name,info,num_temperatures):
        self.name = name
        w = info.split(None,1)
        self.num_species = int(w[0])
        info = w[1].strip()
        w = info.split(None,self.num_species*2)
        info = w[self.num_species*2]
        self.species_tuple = string_to_species(w[0:self.num_species*2])
        self.dbase_species_tuple = self.species_tuple 
        w = info.split(None,num_temperatures)
        self.logKs = w[0:-1]
        info = w[-1]
        w = info.strip().split()
        self.charge = read_float_safe(w[0])
        self.weight = read_float_safe(w[1])  
        self.swapped_species = []
        
    def info(self):
        sys.stdout.write('{} {}'.format(self.name,self.num_species))
        for name, stoich in self.dbase_species_tuple:
            sys.stdout.write(' {} {}'.format(stoich,name))
        for logK in self.logKs:
            sys.stdout.write(' {}'.format(logK))
        sys.stdout.write(' {} {}\n'.format(self.charge,self.weight))
        
class Suite():
    def __init__(self,species_list,dbase_pri_aqueous_dict,dbase_sec_aq_dict,
                 dbase_gas_dict,dbase_mineral_dict,dbase_srfcplx_dict):
        self.primary_aqueous_species = []
        self.secondary_aqueous_species = []
        self.gas_species = []
        self.mineral_species = []
        self.surface_complex_species = []
        for key, p in dbase_pri_aqueous_dict.items():
            if p.match(species_list):
                self.primary_aqueous_species.append(p)
        for key, p in dbase_sec_aq_dict.items():
            if p.match(species_list):
                self.secondary_aqueous_species.append(p)
        for key, p in dbase_gas_dict.items():
            if p.match(species_list):
                self.gas_species.append(p)
        for key, p in dbase_mineral_dict.items():
            if p.match(species_list):
                self.mineral_species.append(p)
        for key, p in dbase_srfcplx_dict.items():
            if p.match(species_list):
                self.surface_complex_species.append(p)
        
    def info(self):
        sys.stdout.write('Primary Aqueous Species: ------\n')
        for p in self.primary_aqueous_species:
            p.info()
        sys.stdout.write('Secondary Aqueous Species: ------\n')
        for p in self.secondary_aqueous_species:
            p.info()
        sys.stdout.write('Gas Species: ------\n')
        for p in self.gas_species:
            p.info()
        sys.stdout.write('Mineral Species: ------\n')
        for p in self.mineral_species:
            p.info()
        sys.stdout.write('Surface Complex Species: ------\n')
        for p in self.surface_complex_species:
            p.info()

def calculate_secondary_species_list(dbase_sec_aq_dict,
                                     user_primary_species,
                                     user_primary_that_are_dbase_secondary_list,
                                     calc_secondary_that_are_dbase_primary_list,
                                     secondary_species_list,
                                     redox_species_list):
    for k,p in dbase_sec_aq_dict.items():
        if p.name in redox_species_list:
            continue
        if p.name in user_primary_species:
            user_primary_that_are_dbase_secondary_list.append(p.name)
        add_species = True
        for name, stoich in p.get_species_tuple():
            if name not in primary_species_dict and \
               name not in calc_secondary_that_are_dbase_primary_list and \
               not name == 'H2O':
                add_species = False
        # skip secondary species already in primary list
        if p.name in primary_species_dict:
            add_species = False
        if add_species:
            if not p.name in calc_secondary_species_list:
                calc_secondary_species_list.append(p.name)

#f = open('test.dat','r')
f = open('pt_database.dat','r')

lists = []
sublist = []
null_count = 0
# remove temperatures
s = f.readline()
w = s.split(' ')
num_temperatures = int(w[2])
temperatures = w[3:num_temperatures]
while(True):
    if null_count == 5:
        break
    s = f.readline()
#    print(s)
    species, info = s.split(' ',1)
    species = species.strip("'")
    info = info.strip()
    if species.startswith('null'):
        null_count += 1
        lists.append(sublist)
        sublist = []
        continue
    if len(species) > 32:
        # skip species names with more than 32 characters
        continue
    sublist.append((species,info))
lists.append(sublist)
sublist = []
f.close()

dbase_pri_aq_dict = {}
for name, info in lists[0]:
    dbase_pri_aq_dict[name] = PrimaryAqueousSpecies(name,info)
del dbase_pri_aq_dict['H2O']
dbase_sec_aq_dict = {}
for name, info in lists[1]:
    dbase_sec_aq_dict[name] = SecondaryAqueousSpecies(name,info,num_temperatures)
dbase_gas_dict = {}
for name, info in lists[2]:
    dbase_gas_dict [name] = GasSpecies(name,info,num_temperatures)
del dbase_gas_dict['H2O(g)']
dbase_mineral_dict = {}
for name, info in lists[3]:
    dbase_mineral_dict[name] = MineralSpecies(name,info,num_temperatures)
del dbase_mineral_dict['H2O(s)']
del dbase_mineral_dict['Ice']
dbase_srfcplx_dict = {}
for name, info in lists[4]:
    dbase_srfcplx_dict[name] = SurfaceComplexSpecies(name,info,num_temperatures)

#user_primary_species = ['H+','CO2(aq)','Ca++']
#user_primary_species = ['CO2(aq)','CaOH+','Ca++','MgOH+','Mn++']
#user_primary_species = ['O2(aq)','H+','SO4--']
#user_primary_species = ['H+','CO2(aq)','Ba++','Ca++','Cl-','Co++','F-',
#                        'Mn++','Mg++','NO3-','K+',
#                        'SiO2(aq)','Na+','Sr++','SO4--']
user_primary_species = ['Al+++',
                        'Ba++','Ca++','Cl-','Co++','F-','H+','HCO3-','K+',
                        'Mg++','Mn++','Na+','NO3-','O2(aq)',
                        'SO4--','SiO2(aq)','Sr++']
#user_primary_species = ['H+','Sn++','NO3-']
redox_species_list = []
redox_species_list = ['NO3-','NO2-','SO4--','HS-']

minerals_to_skip = ['CO2(s)','CO2(s)*','CO2.6.3H2O']

suite = Suite(user_primary_species,
              dbase_pri_aq_dict,dbase_sec_aq_dict,dbase_gas_dict,
              dbase_mineral_dict,dbase_srfcplx_dict)

#suite.info()

# swap gases out of secondary aqueous species, gases and minerals
# MUST NOT SWAP OUT REDOX SPECIES
# gases first to avoid swapping in gases
num_swapped = 1
while num_swapped > 0:
    # use while loop to avoid nested gases
    num_swapped = 0
    for k,p in dbase_gas_dict.items():
        num_swapped += p.swap_species(dbase_gas_dict,redox_species_list)
for k,p in dbase_sec_aq_dict.items():
    p.swap_species(dbase_gas_dict,redox_species_list)
for k,p in dbase_mineral_dict.items():
    p.swap_species(dbase_gas_dict,redox_species_list)
for k,p in dbase_srfcplx_dict.items():
    p.swap_species(dbase_gas_dict,redox_species_list)

# swap secondary aqueous species out of minerals
for k,p in dbase_mineral_dict.items():
    p.swap_species(dbase_sec_aq_dict,redox_species_list)
 
# create a dictionary of all user primary species (from pri and sec in database)
primary_species_dict = {}
calc_secondary_species_list = []
for k,p in dbase_pri_aq_dict.items():
    if p.name in user_primary_species:
        primary_species_dict[p.name] = p
# note: the secondary redox species will overwrite the primary, which is correct
for k,p in dbase_sec_aq_dict.items():
    if p.name in user_primary_species:
        if p.name in redox_species_list:
            continue
        primary_species_dict[p.name] = p

# make list of all dbase primaries that are dbase secondaries (these are likely redox species)
dbase_species_in_both_primary_and_secondary = []
for k,p in dbase_pri_aq_dict.items():
    if k in dbase_sec_aq_dict:
        dbase_species_in_both_primary_and_secondary.append(p.name) 

# find species that are primary in the database but now secondary based on
# use prescribing dbase secondaries as primaries
calc_secondary_that_are_dbase_primary_list = []
user_primary_that_are_dbase_secondary_list = []
for k,p in primary_species_dict.items():
    for name, stoich in p.get_species_tuple():
        if name not in primary_species_dict:
            if name not in calc_secondary_species_list and not name == 'H2O':
                # add dbase primary species in secondary that are not user-specified primary
                calc_secondary_species_list.append(name)
        for k2,p2 in dbase_pri_aq_dict.items():
            if name == p2.name:
                if name not in primary_species_dict:
                    if name not in calc_secondary_that_are_dbase_primary_list:
                        calc_secondary_that_are_dbase_primary_list.append(name)

# add secondaries from dbase secondaries
calculate_secondary_species_list(dbase_sec_aq_dict,
                                 user_primary_species,
                                 user_primary_that_are_dbase_secondary_list,
                                 calc_secondary_that_are_dbase_primary_list,
                                 calc_secondary_species_list,
                                 redox_species_list)

calc_dbase_mineral_species = []
for k,p in dbase_mineral_dict.items():
    add_species = True
    if len(p.species_tuple) == 0:
        # skip mineral without any species in reaction
        add_species = False
    if p.name in minerals_to_skip:
        # skip minerals in minerals_to_skip list
        add_species = False
    for name, stoich in p.get_species_tuple():
        if name not in primary_species_dict and \
           name not in calc_secondary_that_are_dbase_primary_list and \
           not name == 'H2O':
            add_species = False
    if add_species:
        if not p.name in calc_dbase_mineral_species:
            calc_dbase_mineral_species.append(p.name)
         
calc_dbase_srfcplx_species = []
for k,p in dbase_srfcplx_dict.items():
    add_species = True
    if len(p.species_tuple) == 0:
        # skip mineral without any species in reaction
        add_species = False
    for name, stoich in p.get_species_tuple():
        if name not in primary_species_dict and \
           name not in calc_secondary_that_are_dbase_primary_list and \
           not name == 'H2O':
            add_species = False
    if add_species:
        if not p.name in calc_dbase_srfcplx_species:
            calc_dbase_srfcplx_species.append(p.name)            
        
for name in user_primary_species:
    if not name in primary_species_dict:
        sys.stdout.write('Species "{}" not found in database.'.format(name))
        sys.exit(0)
        
def print_indent(string,num_spaces=0):
    indentation = ' ' * num_spaces
    print(indentation + string)

final_pri_aq_dict = {}    
for key in primary_species_dict:
    final_pri_aq_dict[key] = primary_species_dict[key]
final_sec_aq_dict = {}    
for name in calc_secondary_species_list:
#    if name in skip_species_list:
#        continue
    found = False
    for k,p in dbase_sec_aq_dict.items():
        if name == p.name:
            final_sec_aq_dict[name] = p
            found = True
            break
    if not found:
        for k,p in dbase_pri_aq_dict.items():
            if name == p.name:
                final_sec_aq_dict[name] = p
                found = True
                break
    if not found:
        print('{} not found for final_secondary_aqueous_species_dict'.format(name))        
        sys.exit(0)

final_decouple_species_dict = {}
for key in final_pri_aq_dict:
    if key in redox_species_list:
        final_decouple_species_dict[key] = final_pri_aq_dict[key]

final_mineral_dict = {}    
for name in calc_dbase_mineral_species:
#    if name in skip_species_list:
#        continue
    found = False
    for k,p in dbase_mineral_dict.items():
        if name == p.name:
            final_mineral_dict[name] = p
            found = True
            break
    if not found:
        print('{} not found for final_mineral_dict'.format(name))        
        sys.exit(0)
        
final_srfcplx_dict = {}    
for name in calc_dbase_srfcplx_species:
#    if name in skip_species_list:
#        continue
    found = False
    for k,p in dbase_srfcplx_dict.items():
        if name == p.name:
            final_srfcplx_dict[name] = p
            found = True
            break
    if not found:
        print('{} not found for final_srfcplx_dict'.format(name))        
        sys.exit(0)        

# gases last
calc_gas_species = {}
for k,p in final_pri_aq_dict.items():
    p.insert_swapped_gases(calc_gas_species,dbase_gas_dict)
for k,p in final_sec_aq_dict.items():
    p.insert_swapped_gases(calc_gas_species,dbase_gas_dict)
#for k,p in dbase_gas_dict.items():
#    p.insert_swapped_gases(calc_gas_species,dbase_gas_dict)
for k,p in final_mineral_dict.items():
    p.insert_swapped_gases(calc_gas_species,dbase_gas_dict)
for k,p in final_srfcplx_dict.items():
    p.insert_swapped_gases(calc_gas_species,dbase_gas_dict)
    
final_gas_dict = {}    
for name in calc_gas_species:
#    if name in skip_species_list:
#        continue
    found = False
    for k,p in dbase_gas_dict.items():
        if name == p.name:
            final_gas_dict[name] = p
            found = True
            break
    if not found:
        print('{} not found for final_gas_dict'.format(name))        
        sys.exit(0)

def print_dict(title,dictionary,indentation):
    print('{} [{}]'.format(title,len(dictionary)))
    if len(dictionary) > 0:
        for key in sorted(dictionary.keys()):
            print_indent(dictionary[key].print_species(),indentation)
    else:
        print_indent('None',indentation)
        
def write_dict(f,block_title,dictionary):
    if len(dictionary) == 0:
        return
    f.write('  {} ! {}\n'.format(block_title,len(dictionary)))
    for key in sorted(dictionary.keys()):
        f.write('    {}\n'.format(dictionary[key].name))
    f.write('  /\n')

num_spaces = 2
print('')
print_dict('Primary Species:',final_pri_aq_dict,num_spaces)
print_dict('Secondary Species:',final_sec_aq_dict,num_spaces)
print_dict('Redox Species',final_decouple_species_dict,num_spaces)
print_dict('Gas Species:',final_gas_dict,num_spaces)
print_dict('Mineral Species:',final_mineral_dict,num_spaces)
print_dict('Surface Complex Species:',final_srfcplx_dict,num_spaces)

print('Secondary species that are primaries in database:')
for name in calc_secondary_that_are_dbase_primary_list:
    print_indent(name,num_spaces)

print('Species that are primaries and secondaries in database:')    
for name in dbase_species_in_both_primary_and_secondary:
    print_indent(name,num_spaces) 
    
# the number of should match
swapped_num = len(calc_secondary_that_are_dbase_primary_list) - \
              len(user_primary_that_are_dbase_secondary_list)
if swapped_num > 0:
    print('Must add {} primary species to offset {} of the following:'.format(swapped_num,swapped_num))
    print(calc_secondary_that_are_dbase_primary_list)
    
filename = 'chemistry_block.txt'
f = open(filename,'w')
write_dict(f,'PRIMARY_SPECIES',final_pri_aq_dict)
write_dict(f,'SECONDARY_SPECIES',final_sec_aq_dict)
write_dict(f,'DECOUPLED_EQUILIBRIUM_REACTIONS',final_decouple_species_dict)
write_dict(f,'PASSIVE_GAS_SPECIES',final_gas_dict)
write_dict(f,'MINERALS',final_mineral_dict)
#write_dict(f,'SURFACE_COMPLEXES',final_mineral_dict)
f.close()

string = 'Mn'
print('Minerals with "{}" in reaction:'.format(string))    
for k,p in sorted(final_mineral_dict.items()):
    found = False
    for name, stoich in p.get_species_tuple():
        if string in name:
            found = True
            break
    if found:
        print_indent(p.print_species(),2)
            
    
print('done')
