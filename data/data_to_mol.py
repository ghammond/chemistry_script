#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 10:03:46 2021

@author: hamm495
"""
import sys
sys.path.append('../scripts')
import numpy as np
from extract_column_data import *



def mass_to_molarity(in_filename,out_filename,list_):

    species = list_[0]
    pv,values = ReadColumnData(in_filename,list_[0])
    pv_per_day = 6.65
    times = pv/pv_per_day
    data = np.zeros((len(pv),len(list_)),dtype='float')
    ispecies = 0
    for species in list_:
        print(species)
        pv,values = ReadColumnData(in_filename,species)
        for i in range(len(values)):
            data[i][ispecies] = values[i]
        ispecies += 1
    
    f = open(out_filename,'w')
    f.write('Time [d]')
    i = 1
    for species in list_:
        i += 1
        f.write(', {}~{}'.format(i,species))
    f.write('\n')
    for j in range(len(times)):
        f.write('{}'.format(times[j]))
        for i in range(data.shape[1]):
            value = data[j][i]
            if value < -998.:
                f.write(', ND')
            else:
                f.write(', {}'.format(value))
        f.write('\n')
    f.close()
    
def get_outfilename(filename,tag):
    outfilename = data_filename.replace('.csv','_'+tag+'.csv')
    print('\nWriting to {}'.format(outfilename))
    return outfilename

if __name__ == "__main__":
    data_filename = './Column1_Effluent.csv'
    outfilename = get_outfilename(data_filename,'molarity')
    list_ = ['Al+++','Ba++','Ca++','Cl-','Co++','Mg++','Mn++','Mo++','NO3-',
             'NO2-','K+','SiO2(aq)','Na+','Sr++','SO4--','S--']
    mass_to_molarity(data_filename,outfilename,list_)
    outfilename = get_outfilename(data_filename,'pH')
    list_ = ['pH']
    mass_to_molarity(data_filename,outfilename,list_)
    outfilename = get_outfilename(data_filename,'carbon')
    list_ = ['HCO3-','C']
    mass_to_molarity(data_filename,outfilename,list_)
    print('done')
