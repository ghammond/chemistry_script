#!/bin/sh

REPO_PATH=$(git rev-parse --show-toplevel)
python3 $REPO_PATH/scripts/driver.py params.in results.out
