import sys
import numpy as np

column_dictionary = {}
column_dictionary['Al+++'] = 2
column_dictionary['Ba++'] = 3
column_dictionary['Ca++'] = 4
column_dictionary['Cl-'] = 5
column_dictionary['Co++'] = 6
column_dictionary['Mg++'] = 7
column_dictionary['Mn++'] = 8
column_dictionary['Mo++'] = 9
column_dictionary['NO3-'] = 10
column_dictionary['NO2-'] = 11
column_dictionary['K+'] = 12
column_dictionary['SiO2(aq)'] = 13
column_dictionary['Na+'] = 14
column_dictionary['Sr++'] = 15
column_dictionary['SO4--'] = 16
column_dictionary['S--'] = 17
column_dictionary['pH'] = 19
column_dictionary['H+'] = 19
column_dictionary['HCO3-'] = 21
column_dictionary['C'] = 22

grams_per_mole = {}
grams_per_mole['Al+++'] = 26.982
grams_per_mole['Ba++'] = 137.33
grams_per_mole['Ca++'] = 40.078
grams_per_mole['Cl-'] = 35.45
grams_per_mole['Co++'] = 58.933
grams_per_mole['Mg++'] = 24.305
grams_per_mole['Mn++'] = 54.938
grams_per_mole['Mo++'] = 95.95
grams_per_mole['NO3-'] = 62.004
grams_per_mole['NO2-'] = 46.005
grams_per_mole['K+'] = 39.098
grams_per_mole['SiO2(aq)'] = 60.083
grams_per_mole['Na+'] = 22.990
grams_per_mole['Sr++'] = 87.62
grams_per_mole['SO4--'] = 96.056
grams_per_mole['S--'] = 32.06
grams_per_mole['pH'] = 1.
grams_per_mole['H+'] = 1.008
grams_per_mole['HCO3-'] = 61.016
grams_per_mole['C'] = 12.011

def ReadColumnData(filename,species):
    icolumn = column_dictionary[species]
    try:
        f = open(filename,'r')
    except:
        sys.exit('File {} not found'.format(filename))
    data = []
    pv_col = 0
    if icolumn == 1:
        print('Cannot choose column 1 as it is a PV column')
        raise
    elif icolumn > 1 and icolumn <= 17:
        pv_col = 1
    elif icolumn == 18:
        print('Cannot choose column 18 as it is the PV column for pH')
        raise
    elif icolumn == 19: # pH
        pv_col = 18
    elif icolumn == 20:
        print('Cannot choose column 20 as it is the PV column for Carbon')
        raise
    elif icolumn >= 21 and icolumn <= 22:
        pv_col = 20
    else:
        print('Column # must be less than 22')
        raise
    pv_col -= 1
    data_col = icolumn-1
    for i in range(7):
        f.readline()
    while True:
        line = f.readline()
        if len(line) == 0:
            break
        w = line.split(',')
        if len(w[pv_col].strip()) == 0:
            break
        if not w[data_col].strip() == 'ND':
            data.append((float(w[pv_col]),float(w[data_col])))
        else:
            data.append((float(w[pv_col]),-999.))
    f.close()
    pv = np.zeros(len(data))
    conc = np.zeros(len(data))
    i = 0
    for tpl in data:
        pv[i] = tpl[0]
        conc[i] = tpl[1]
        i += 1
    if species == 'H+':
        for i in range(len(conc)):
            conc[i] = 10.**-conc[i]
    elif not species == 'pH':
        # convert ug/L to mol/L
        for i in range(len(conc)):
            if conc[i] > 0.:
                conc[i] *= 1.e-6 / grams_per_mole[species]
    return pv,conc

if __name__ == "__main__":
    try:
        filename = 'Column1_Effluent.csv'
        pv,conc = ReadColumnData(filename,'H+')
        print(pv,conc)
    except Exception as error:
        print(str(error))
        sys.exit(1)
    print('done')
