#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 16:45:22 2021

@author: hamm495
"""
import os
import sys

try:
    pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
    print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
    sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import subprocess
import argparse
import numpy as np
import pflotran as pft
from error_integrator import *
from extract_column_data import *

parser = argparse.ArgumentParser(description='Process arguments')
parser.add_argument('--dakota-parameters-filename',
                    default=None,
                    help='Name of Dakota-generated parameter file.')
parser.add_argument('--dakota-results-filename',
                    default='results.out',
                    help='Name of results file returned to Dakota.')
parser.add_argument('--pflotran-prefix',
                    default=None,
                    help='Prefix of PFLOTRAN input file. Also used for '
                    'locating observation results.')
parser.add_argument('--experimental-results-filename',
                    default=None,
                    help='Name of file with experimental results')
parser.add_argument('--default-parameters',
                    default=None,
                    help='Name of file with default parameters')

options = parser.parse_args()

dict_ = {}
# read defaults
f = open(options.default_parameters,'r')
for line in f:
    words = line.strip().split()
    dict_[words[0]] = words[1]
f.close()

# read parameters from Dakota
if options.dakota_parameters_filename:
    f = open(options.dakota_parameters_filename,'r')
    num_variables = int(f.readline().strip().split()[0])
    for i in range(num_variables):
        words = f.readline().strip().split()
        dict_[words[1]] = words[0]
    f.close()

if options.pflotran_prefix == None:
    sys.exit('No input prefix provided. Please include --pflotran-prefix '
             'to include the string used for -input_prefix with PFLOTRAN.')
filename = options.pflotran_prefix + '.template'
fin = open(filename,'r')
fout = open(filename.split('.')[0]+'.in','w')
for line in fin:
    for key,value in dict_.items():
        line = line.replace(key,value)
    fout.write(line)
fin.close()
fout.close()

command = []
command.append(pflotran_dir+'/src/pflotran/pflotran')
command.append('-input_prefix')
command.append(options.pflotran_prefix)
filename = 'pflotran.stdout'
proc = subprocess.run(command,
                      shell=False,
                      stdout=open(filename,'w'),
                      stderr=subprocess.STDOUT)

m = -999.
a = -999.
if options.experimental_results_filename:
    species = 'Ca++'
    pv,values1 = ReadColumnData(options.experimental_results_filename,species)
    pv_per_day = 6.65
    times1 = pv/pv_per_day

    data = pft.Dataset(options.pflotran_prefix+'-obs-0.pft',1,1)
    icol = -999
    i = 0
    for string in data.variables:
        i += 1
        if species in string:
            icol = i
            break
    if icol == -999:
        sys.exit('Species not found in PFLOTRAN observation file.')
    data = pft.Dataset(options.pflotran_prefix+'-obs-0.pft',1,icol)
    times2 = data.get_array('x')
    values2 = data.get_array('y')

    times1 = np.insert(times1,0,0.)
    values1 = np.insert(values1,0,values2[0])

    integrator = ErrorIntegrator()
    m,a = integrator.calculate_error(times1,values1,times2,values2)
if len(options.dakota_results_filename) > 0:
    fout = open(options.dakota_results_filename,'w')
    fout.write('{}'.format(m))
    fout.close()
else:
    print(m,a)
